#include "GPS.h"
#include <string.h>
#include <stdlib.h>

struct gps_data_struct gps_data ;

void parseGPS(char* in_buffer){
	char tmp_buffer[100];
	int tmp_idx=0;
	int in_idx=0;
	unsigned char section_idx=0;
	while(in_idx<100 && in_buffer[in_idx] && section_idx!=201){ //We'll never have 200+ sections of gps data
		if (in_buffer[in_idx]!=','){ //Read in section data
			tmp_buffer[tmp_idx]=in_buffer[in_idx];
			tmp_idx++;
		}else{ //End of section, terminate tmp_buffer and set the variable of the section
			tmp_buffer[tmp_idx]=0;
			tmp_idx=0;
			switch(section_idx){
				case 0: //Check GPS packet
					if (!(tmp_buffer[3]=='G' && tmp_buffer[4]=='G' && tmp_buffer[5]=='A')){
						section_idx=200; //Leave next loop, this isn't the packet we want
					}
				case 1:
					strcpy(gps_data.time,tmp_buffer); break; //GPS Time
				case 2:
					strcpy(gps_data.lat,tmp_buffer); break; //GPS Latitude
				case 3:
					strcat(gps_data.lat,tmp_buffer); break; //Append cardinal direction
				case 4: 
					strcpy(gps_data.lng,tmp_buffer); break; //GPS Longitude
				case 5:
					strcat(gps_data.lng,tmp_buffer); break; //Again, cardinal direction
				case 6: 
					gps_data.fix=atoi(tmp_buffer);
				case 7: 
					strcat(gps_data.sats,tmp_buffer); break; //GPS satellites tracked
				case 9:
					strcpy(gps_data.alt,tmp_buffer); break; //GPS Altitude
			}
			section_idx++;
		}
		in_idx++;
	}
}

