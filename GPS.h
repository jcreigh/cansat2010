#ifndef __GPS_H__
#define __GPS_H__

struct gps_data_struct{
	char fix;
	char time[7];
	char lat[11];
	char lng[12];
	char sats[3];
	char alt[10];
};

void parseGPS(char* in_buffer);

#endif //__GPS_H__
