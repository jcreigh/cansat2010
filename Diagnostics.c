/*

	Various commands for outputting and sending
	diagnostic infomation about self.

	Such as:
		Current state.
		Sensor information
		Auto send diagnostics 
			For use with program on laptop?
		Radio
		Chronos
		SD card
		GPS
		Camera


	*/

#include <avr/io.h>
#include <avr/interrupt.h> 
#include "Diagnostics.h"
#include "USART.h"

#define STATUS_SLOTS 8
unsigned char status_idx=0;
unsigned char Status[STATUS_SLOTS];
unsigned char status_tmr=0;


void initStatus(){
	LED_DDR |= _BV(LED_PIN);
	LED_PORT &= ~_BV(LED_PIN); // Initially on
	setStatus(OKAY);
}

void setStatus(int newstatus){
	switch (newstatus){
		case OKAY:
			Status[0]=100;
			Status[1]=100;
			Status[2]=0;
			break;
		case SDFAIL:
			Status[0]=50;
			Status[1]=10;
			Status[2]=0;
			break;
		case STORAGE_READ_ERR:
			Status[0]=25;
			Status[1]=10;
			Status[2]=25;
			Status[3]=10;
			Status[4]=50;
			Status[5]=10;
			Status[6]=0;
			break;
		case STORAGE_WRITE_ERR:
			Status[0]=25;
			Status[1]=10;
			Status[2]=50;
			Status[3]=10;
			Status[4]=50;
			Status[5]=10;
			Status[6]=0;
			break;
		case CAMERA_NAK:
			Status[0]=10;
			Status[1]=25;
			Status[2]=10;
			Status[3]=50;
			Status[4]=0;
			break;
		case CAMERA_UNSYNCABLE:
			Status[0]=10;
			Status[1]=25;
			Status[2]=10;
			Status[3]=25;
			Status[4]=10;
			Status[5]=50;
			Status[6]=0;
			break;
		default: //Status fail! 
			Status[0]=100;
			Status[1]=10;
			Status[2]=10;
			Status[3]=10;
			Status[4]=0;
	}
}

void checkStatus(){
	// Call me every 10ms
	status_tmr++;
	if (status_tmr>=*(Status+status_idx)){
		LED_PORT ^= _BV(LED_PIN);
		status_idx++;
		status_tmr=0;
		if (status_idx == STATUS_SLOTS || !*(Status+status_idx)){
			status_idx=0;
		}
	}
}

