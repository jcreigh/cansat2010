#ifndef __DIAGNOSTICS_H__
#define __DIAGNOSTICS_H__

#define OKAY 0
#define SDFAIL 1
#define STORAGE_READ_ERR 2
#define STORAGE_WRITE_ERR 3
#define CAMERA_NAK 4
#define CAMERA_UNSYNCABLE 5

#define LED_PORT PORTC
#define LED_PIN PC0
#define LED_DDR DDRC

void initStatus();
void setStatus(int newstatus);
void checkStatus();

#endif
