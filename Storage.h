#ifndef __STORAGE_H__
#define __STORAGE_H__

void initStorage();
void log_writeLine(char line[]);
int log_readLine(char line[]);
void log_seekLine(int linenum);
void log_open();
void log_close();
void log_sync();
char log_opened();

#endif //__STORAGE_H__
