#include <avr/io.h>
#include "Sensors.h"
#include "USART.h"
//#include <stdio.h>

// Sensor input ADC

void initSensors(){
	//This probably initializes
	//the ADC the way we want
	ADC_DDR=0; //All input
	ADC_PORT=0;
	ADMUX|=_BV(REFS0); // Use Vcc as reference
	ADCSRA|=_BV(ADPS2)|_BV(ADPS1); //Set prescaler to 64
	ADCSRA|=_BV(ADEN); //Enable ADC

	return;
}

int readADC(char sensor){
	ADMUX&=~0b111;                //Clear first three bits
	ADMUX|=sensor;                // Magic
	ADCSRA |= _BV(ADSC);          // Discard first conversion
	while (ADCSRA & _BV(ADSC));  	// wait until conversion is done
	int x = ADCH;
	ADCSRA |= _BV(ADSC);          // start single conversion
	while (ADCSRA & _BV(ADSC));  	// wait until conversion is done
	x = ADCL;
	x |= ADCH<<8;
	return x;
}

double convertADC(char sensor, int value){
// This contains the ADC->Pressure, Voltage, Acceleration, etc.
	double voltin = value * 3.3/1023;
	switch (sensor){
		case PRESSURE:
			return (33.67 * (voltin+.03135));
		case TEMP:
			return (voltin-.4)/(.0195);
		case VOLTAGE:
			return (3.3 * (.009*voltin-.095));  // Where are these numbers from
		case ACCEL_X:
		case ACCEL_Y: // Do all three of these use same formula?
		case ACCEL_Z:
			return (voltin-1.65)*ACCEL_PRECISION/1.2;
	}
	return 0;
}



