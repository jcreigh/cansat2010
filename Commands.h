
#define MAXPKTSIZE 0xF8

//Command States
#define IDLE 0
#define GETPACKETHEAD 1
#define GETPACKETBODY 2

//Cansat States
#define OFF 0
#define PREFLIGHT 1
#define LAUNCHED 2
#define DEPLOYED 3
#define POSTFLIGHT 4
#define TAKEPHOTO 5




void initCommands();
void getCommand();
void parseCommand();
void commandsLoop();
int packetizeAccel();
