#include "Telemetry.h"
#include "GPS.h"
#include <string.h>
#include <stdio.h>
//#include <stdlib.h>
#include "Radio.h"
#include "Sensors.h"
#include "Storage.h"
#include "USART.h"

struct enviro_data_struct enviro;
extern struct gps_data_struct gps_data;
extern unsigned long chronos;
extern char CansatStatus;

int average_total_accel=0;
char getSensor=0;

void createTelemetryPkt(char* string){
	sprintf(string,"%s,%s,%s,%s,%s,%f,%u,%u,",
			gps_data.time,
			gps_data.lat,
			gps_data.lng,
			gps_data.alt,
			gps_data.sats,
			((double)enviro.alt)/10, // Decameters to meters
			enviro.temp[0],
			enviro.voltage[0]
	);
	createPacket(string);
}

void sensorLoop(){
	if (chronos % 200==0){
		sendString(RADIO,"Rawr\n");
	}
	if (getSensor & ENVIRO){
		sendString(RADIO,"Enviro!!!\n");
		getSensor &= ~ENVIRO;
		sensorAverage(enviro.voltage,readADC(VOLTAGE));
		sensorAverage(enviro.temp,readADC(TEMP));
		sensorAverage(enviro.pressure,readADC(PRESSURE));
		char miscbuf[20];
		sprintf(miscbuf,"V %u\nT %u\nP %u\n",enviro.voltage[0],enviro.temp[0],enviro.pressure[0]);
//		sendString(RADIO,miscbuf);
		log_writeLine(miscbuf);
	}
	if (getSensor & GPSPKT){
		sendString(RADIO,"GPS!!!\n");
		getSensor &= ~GPSPKT;
	//	char gpsbuf[30];
		char b = getBlocking();
		setBlocking(NONBLOCK);
	//	if (readLine(GPS,gpsbuf)){
	//		parseGPS(gpsbuf);
	/*		sprintf(gpsbuf,"!G %s,%s,%s,%s,%s !!\n",
					gps_data.time,
					gps_data.lat,
					gps_data.lng,
					gps_data.alt,
					gps_data.sats);
			sendString(RADIO, gpsbuf);
			log_writeLine(gpsbuf);*/
	//	}
		setBlocking(b);
	}
	if (getSensor & ACCELS){
		getSensor &= ~ACCELS;
		sensorAverage(enviro.accel_y,readADC(ACCEL_Y));
		sensorAverage(enviro.accel_x,readADC(ACCEL_X));
		sensorAverage(enviro.accel_z,readADC(ACCEL_Z));
		char accbuf[20];
		sprintf(accbuf,"C %lu\nA %u %u %u\n",chronos*5,enviro.accel_x[0], enviro.accel_y[0],enviro.accel_z[0]);
		log_writeLine(accbuf);
	}
}


void getSensors(){ 
	// Chronos is handled by Chronos.c 

	getSensor |= ACCELS;
	
	if (chronos % 10==0){ // Every 100ms, supposedly
		sendString(RADIO,"Get enviro!\n");
		getSensor |= ENVIRO;
	}
	
	if (chronos % 100==0){ // Twice a second
		sendString(RADIO,"Get GPS!\n");
		getSensor |= GPSPKT;
	}
}

void sensorAverage(int sensor[], int value){
	sensor[0]=value;
/*	sensor[0]=(sensor[0]*AVGCOUNT-sensor[sensor[1]+2]+value)/AVGCOUNT;
	sensor[sensor[1]+2]=value;
	sensor[1]=(sensor[1]+1)%AVGCOUNT;*/
}


void logGPS(){ // Call me once a second
	char buf[50];
	*buf=0;
	strcat(buf,gps_data.time);  //Time
	strcat(buf,gps_data.lat);   //Latitude
	strcat(buf,gps_data.lng);  //Longitude
	strcat(buf,gps_data.alt);  //Altitude
}

void genAltitude(){
	// Here goes pressure + temp -> altitude eqns
}


void sendTelemetry(){ // Call me every 5 seconds
	char buf[120];
	createTelemetryPkt(buf);
	sendString(RADIO,buf);
	log_sync();
}
