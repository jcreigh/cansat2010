#include "Radio.h"
#include <string.h>

void lairdPkt(char *string){
   string[0]=0x81; // Fixed Header
   string[1]=' ';   // Filler for now, will be length
   string[2]=0x08; // More
   string[3]=0x04; // Headers

   // StationAddress
   strcat(string,stationAddress);
}

void setlairdPktSize(char *string){
   string[1]=strlen(string)-7;
}

void createPacket(char *string){
	char x=strlen(string);
	int i=x+4;
	for (;i>=4;i--){
		string[i]=string[i-4]; // shift string over 4. Assume there's room
	}
	lairdPkt(string); // Set first 4 bytes
	setlairdPktSize(string); // Set size
}

