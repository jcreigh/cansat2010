#ifndef __CAMERA_H__
#define __CAMERA_H__

#define IDLE 0
#define UNSYNCED 1
#define INITIALIZING 2
#define SYNCED 3
#define SNAPPING 4
#define GETSNAP 5
#define DOWNLOAD 6
#define DLFAIL 7

#define PKG_SIZE 64

#define camTimedout() (cam_timeout > 0 && chronos > cam_timeout)
#define setCamTimeout(to) cam_timeout = chronos + to;

void cameraConnect();	
void cameraLoop();
void getCamCommand(unsigned char str[]);
void parseCamCommand(unsigned char str[]);
void initCamera();
void sendInit();
void sendACK(char cmdID, unsigned int pkgID);
void sendSYNC();
void takeSnap();
void getSnap();
void download();
char checksum(char buf[], int length);
void sendCmd(char c[]);
void openCamFile();

#endif // __CAMERA_H__

