#ifndef __USART_H__
#define __USART_H__

#define GPS_USART_BAUDRATE 4800
#define GPS_BAUD_PRESCALE (((F_CPU / (GPS_USART_BAUDRATE * 16UL))) - 1) 

#define CAMERA_USART_BAUDRATE 14400
#define CAMERA_BAUD_PRESCALE (((F_CPU / (CAMERA_USART_BAUDRATE * 16UL)))-1)

#define CAMERA_BAUD_PRESCALE1 (((F_CPU / (9600 * 16UL)))-1)
#define CAMERA_BAUD_PRESCALE2 (((F_CPU / (14400 * 16UL)))-1)
#define CAMERA_BAUD_PRESCALE3 (((F_CPU / (19200 * 16UL)))-1)
#define CAMERA_BAUD_PRESCALE4 (((F_CPU / (28800 * 16UL)))-1)


#define TX0BUFFERSIZE 100
#define RX0BUFFERSIZE 400

#define RADIO_USART_BAUDRATE 9600
#define RADIO_BAUD_PRESCALE (((F_CPU / (RADIO_USART_BAUDRATE * 16UL))) - 1) 

#define TX1BUFFERSIZE 100
#define RX1BUFFERSIZE 150

#define GPSCAM_PORT PORTC
#define GPSCAM_DDR DDRC
#define GPS_ENABLE PC0

#define RADIO 1
#define CAMERA 2
#define GPS 3

#define NONBLOCK 0
#define BLOCK 1

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

void setBlocking(char b);
char getBlocking();
void sendChar(char port, char c);
void sendString(char port, char string[]);
void sendData(char port, char * string, int count);
void readData(char port, char * string, int count);
char readChar(char port);
int readLine (char port, char string[]);
void initUSART(void) ;
void enableCamera();
void enableGPS();
int unread(char port);

#endif //__USART_H__
