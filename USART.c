#include "USART.h"

volatile char	TX0_buffer[TX0BUFFERSIZE]; //TX0 // GPS/CAMERA
volatile int 	TX0_Rd_Idx=0;
int 				TX0_Wr_Idx=0;

volatile char 	RX0_buffer[RX0BUFFERSIZE]; //RX0 // GPS/CAMERA
volatile int 	RX0_Rd_Idx=0;
int 				RX0_Wr_Idx=0;

volatile char 	TX1_buffer[TX1BUFFERSIZE]; //TX1 // RADIO
volatile int 	TX1_Rd_Idx=0;
int 				TX1_Wr_Idx=0;

volatile char 	RX1_buffer[RX1BUFFERSIZE]; //RX1 // RADIO
volatile int 	RX1_Rd_Idx=0;
int 				RX1_Wr_Idx=0;


char Blocking=NONBLOCK;

void setBlocking(char b){
	Blocking=b;
}

char getBlocking(){
	return Blocking;
}

void sendChar(char port, char c){
	if (port == CAMERA) { // Camera is on usart port 0.
		TX0_buffer[TX0_Wr_Idx]=c; //write char at next buffer index
		TX0_Wr_Idx=(TX0_Wr_Idx+1)%TX0BUFFERSIZE; //increment or wrap around TX1_Wr_Idx
		UCSR0B|=_BV(UDRIE0); //Once a character is written to the TX buffer, the TX buffer is no longer empty.
	}else if (port == RADIO){
		TX1_buffer[TX1_Wr_Idx]=c; //write char at next buffer index
		TX1_Wr_Idx=(TX1_Wr_Idx+1)%TX1BUFFERSIZE; //increment or wrap around TX1_Wr_Idx
		UCSR1B|=_BV(UDRIE1); //Once a character is written to the TX buffer, the TX buffer is no longer empty.
	                     //Therefore we can re-enable the data-register-empty interrupt.
	} 
}

void sendString(char port, char string[]){
	int i=0;
	while(string[i]){
		sendChar(port, string[i]);
		i++;
	}
}

void sendData(char port, char * string, int count){
	int i=count;
	while (i--){
		sendChar(port,*(string++));
	}
}

void readData(char port, char * string, int count){
	int i=count;
	while(i--){
		*(string++)=readChar(port);
	}
}

char readChar(char port){
	char t=0;
	if (port == GPS || port== CAMERA){
		while (Blocking && RX0_Rd_Idx==RX0_Wr_Idx){} // If blocking, wait until we have an unread byte
		if (RX0_Rd_Idx!=RX0_Wr_Idx) { 
			t=RX0_buffer[RX0_Rd_Idx]; //check to see if there is a character in the buffer
			RX0_buffer[RX0_Rd_Idx]=0; //set the buffer value at the current index to 0 (empty)
			RX0_Rd_Idx=(RX0_Rd_Idx+1)%RX0BUFFERSIZE; //increment or wrap around RX0_Rd_Idx
		}
	}else if (port == RADIO){
		while (Blocking && RX1_Rd_Idx==RX1_Wr_Idx){} // If blocking, wait until we have an unread byte
		if (RX1_Rd_Idx!=RX1_Wr_Idx) { 
			t=RX1_buffer[RX1_Rd_Idx];
			RX1_buffer[RX1_Rd_Idx]=0; 
			RX1_Rd_Idx=(RX1_Rd_Idx+1)%RX1BUFFERSIZE; 
		}

	}

	return t;
}

int readLine (char port, char string[]){
	char x=0;
	int i=0;
	if (Blocking == NONBLOCK){ // Check to see if there's a newline
		int bufsize;
		if (port == RADIO){
			bufsize=RX1BUFFERSIZE;
		}else{
			bufsize=RX0BUFFERSIZE;
		}
		for (i=1;i<=bufsize; i++){
			if (port == RADIO){
				int curidx=(i+RX1_Rd_Idx)%RX1BUFFERSIZE;
				if (curidx==RX1_Rd_Idx){
					string[0]=0;
					return 0 ; // Thar be no newline
				}
				if (RX1_buffer[curidx]=='\n'){
					break;
				}
			}else{
				int curidx=(i+RX0_Rd_Idx)%RX0BUFFERSIZE;
				if (curidx==RX0_Rd_Idx){
					string[0]=0;
					return 0; // Thar be no newline
				}
				if (RX0_buffer[curidx]=='\n'){
					break;
				}
			}
		}
	}
					
	while (x!='\n'){
		if(x){
			string[i]=x;
			i++;
		}
		x=readChar(port);
	}
	string[i]=0;
	return i;
}

int unread(char port){
	if (port == GPS || port== CAMERA){
		return (RX0BUFFERSIZE + RX0_Wr_Idx-RX0_Rd_Idx)%RX0BUFFERSIZE; 
	}else if (port == RADIO){
		return (RX1BUFFERSIZE + RX1_Wr_Idx-RX1_Rd_Idx)%RX1BUFFERSIZE; 
	}
	return 0;
}

void initUSART(void) {
	//Initialize USART for GPS
	enableGPS();
	UCSR0B |= _BV(RXEN0) | _BV(TXEN0);     // Enable TX and RX on USART0 (camera needs TX)
	UCSR0C |= _BV(UCSZ00) | _BV(UCSZ01);   // 8-bit character size
	UCSR0B |= _BV(UDRIE0) | _BV(RXCIE0);   // Set Enable interrupt for when GPS receiving completes, plus TX for camera

	//Initialize USART for Radio
	UCSR1B |= _BV(RXEN1) | _BV(TXEN1);     // Enable TX and RX to and from radio
	UCSR1C |= _BV(UCSZ10) | _BV(UCSZ11);   // 8-bit character size
	UCSR1B |= _BV(UDRIE1) | _BV(RXCIE1); 	// Set Enable same interrupt for radio, plus a TX interrupt

	UBRR1L = RADIO_BAUD_PRESCALE;          // Set magical number for Radio baud rate
	UBRR1H = (RADIO_BAUD_PRESCALE >> 8);   // Defined in USART.h


	sei(); // Enable interrupts
}

void enableCamera(){
	// Set baud rate
	UBRR0L = CAMERA_BAUD_PRESCALE;        // Set magical number for Camera baud rate
	UBRR0H = (CAMERA_BAUD_PRESCALE >> 8); // Defined in USART.h

	// Disable parity
	UCSR0C &= ~(_BV(UPM01) | _BV(UPM00));

	//Enable 8 bit
	UCSR0B &= ~ _BV(UCSZ02);
	UCSR0C |= _BV(UCSZ00) | _BV(UCSZ01);

	//This might be backward
	GPSCAM_PORT &= ~_BV(GPS_ENABLE); //Pull high to disable
}

void enableGPS(){
	UBRR0L = GPS_BAUD_PRESCALE;            // Set magical number for GPS baud rate
	UBRR0H = (GPS_BAUD_PRESCALE >> 8);     // Defined in USART.h
	//This might be backward
	GPSCAM_PORT |= _BV(GPS_ENABLE); //Pull low to enable
}


ISR(USART0_RX_vect){ //GPS/Camera Receive ISR
	RX0_buffer[RX0_Wr_Idx]=UDR0; //Write character to GPS RX buffer
	RX0_Wr_Idx=(RX0_Wr_Idx+1)%RX0BUFFERSIZE; //Increment or wrap around index
}

ISR(USART0_UDRE_vect){ //Camera Transmit ISR
	if (TX0_Rd_Idx != TX0_Wr_Idx){ //If character value at the current index is not 0
		UDR0 = TX0_buffer[TX0_Rd_Idx];
		TX0_buffer[TX0_Rd_Idx] = 0; //Set current index in TX buffer to 0 (empty)
		TX0_Rd_Idx = (TX0_Rd_Idx+1)%TX0BUFFERSIZE; //Increment or wrap around index
	}else{ //If current index is points to a '0' (empty)
		UCSR0B&=~_BV(UDRIE0); //This means there is no data left to transmit, so we disable 
		                      //the data register empty interrupt				
	}
}

ISR(USART1_UDRE_vect){ //Radio Transmit ISR
	if (TX1_Rd_Idx != TX1_Wr_Idx){ //If we have unsent characters
		UDR1 = TX1_buffer[TX1_Rd_Idx];
		TX1_buffer[TX1_Rd_Idx] = 0; //Set current index in TX buffer to 0 (empty)
		TX1_Rd_Idx = (TX1_Rd_Idx+1)%TX1BUFFERSIZE; //Increment or wrap around index
	}else{
		UCSR1B&=~_BV(UDRIE1); //This means there is no data left to transmit, so we disable 
		                      //the data register empty interrupt				
	}
}

ISR(USART1_RX_vect){ //Radio Receive ISR
	RX1_buffer[RX1_Wr_Idx]=UDR1; //Write character to Radio RX buffer
	RX1_Wr_Idx=(RX1_Wr_Idx+1)%RX1BUFFERSIZE; //Increment or wrap around index
}


