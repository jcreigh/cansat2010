/*
	
	Handle parsing commands received over the radio
	and adjust state machine accordingly
	
	init()
	commandsLoop()
	getCommand() // This is used in Camera.c.
	             // rename that one
	parseCommand() // Same.

	char CansatState

	*/


#include "Commands.h"
#include "USART.h"
#include "Radio.h"
#include "Storage.h"
#include <string.h>
#include <stdio.h>

char CommandState;
char packsize;
char cmdPkt[MAXPKTSIZE];

char CansatState;


void initCommands(){
	CansatState=PREFLIGHT;
	CommandState=IDLE;
}

void commandsLoop(){
	getCommand();
	parseCommand();
}

void getCommand(){
	if (unread(RADIO)){
#ifdef USERADIO
		switch(CommandState){
			case IDLE:
				{
					if (readChar(RADIO) == 0x81){
						CommandState = GETPACKETHEAD;
					}
				}
				break;
			case GETPACKETHEAD:
				{
					if (!packsize){
						packsize=readChar(RADIO);
						if (packsize > MAXPKTSIZE){ //error, ignore this packet :(
							cameraState=IDLE;
							packsize=0;
						}
					}else{
						if (unread(RADIO)>1){
							readChar(RADIO); // Throw out next two packets
							readChar(RADIO);
							CommandState=GETPACKETBODY;
						}
					}
				}
				break;
			case GETPACKETBODY:
				{
					if (unread(RADIO)>packsize){ // Read in packet all at once
						getData(RADIO,cmdPkt,packsize);
						cmdPkt[packsize]=0; // Null terminatey goodness
						CommandState=IDLE;
						packsize;
					}
					break;
				}
		}
#else
		char b=getBlocking();
		readLine(RADIO,cmdPkt);
		setBlocking(b);
#endif
	}
}

void parseCommand(){
	if (cmdPkt[0]){ // We have a packet ready to parse
		if (!strcmp(cmdPkt,"OFF")){
			CansatState=OFF;
		}else if(strcmp(cmdPkt,"JPx")){ // Photo take
			CansatState=TAKEPHOTO;
		}else if (!strcmp(cmdPkt,"ACL,0")){
			char buf[15];
			//sprintf(buf,"ACD,%u",packetizeAccel());
			createPacket(buf);
			sendData(RADIO,buf,strlen(buf));
		}
		
	}
}


int packetizeAccel(){
	// Slooow
	log_seekLine(0);
	char buf[100];
	long accelnum=0; // You never know. We might need 4 hours of accel data!
	while (log_readLine(buf)!=0){
		if (buf[0]=='A'){
			accelnum++;
		}
	}
	return accelnum
		*(8+1+5) // 14 bytes max per accel minipacket
	/75; // Don't fill up the packet, just in case

}
