#include "Camera.h"
#include "USART.h"
#include "Diagnostics.h"
#include "util/delay.h"
#include "main.h"
#include "SD/SD.h"
#include "SD/ff.h"
#include "Commands.h"
#include "Radio.h"
//#include <stdio.h>

extern unsigned long chronos;
extern char CansatState;


char CameraState=0;
char cmdbuffer[100];

char sync_attempts=0;
char cambaud=0;

unsigned int pkgnum=0;
unsigned long numofpkgs=0;
char dlbuf[PKG_SIZE];
unsigned int buf_wr_idx=0;
unsigned long imagesize=0;
unsigned long downloaded=0;

char dl_timeout=0;
unsigned long cam_timeout=0;
char lastcmd[6];

FIL snapfile;
extern FATFS fs;

char snapfilename[15];

void cameraLoop(){
	switch (CameraState){
		case UNSYNCED:
			setStatus(CAMERA_UNSYNCABLE);
			sendSYNC();	
			delayms(50);
			break;
		case INITIALIZING:
			sendInit();
			delayms(150);
			break;
		case SYNCED:
			sync_attempts=0;
			CameraState=INITIALIZING;
			break;
		case SNAPPING:
			delayms(500);
			takeSnap();
			break;
		case DOWNLOAD:
			delayms(125);
			break;
		default:
			delayms(500);
			break;
	}
	if (CameraState!=DOWNLOAD){
		unsigned char cmd[6];
		getCamCommand(cmd);
		parseCamCommand(cmd);
		if (camTimedout()){
			sendCmd(lastcmd);
		}
	}else{
		download();
	}


}

void download(){
	int i=unread(CAMERA);
	setCamTimeout(0);
	if (i>0 && CameraState==DOWNLOAD){
		dl_timeout=0;
		char b=getBlocking();
		setBlocking(NONBLOCK);
		while (i-- && buf_wr_idx<PKG_SIZE){
			dlbuf[buf_wr_idx++]=readChar(CAMERA);
		}
		setBlocking(b);
		//		unsigned int pid = (unsigned int)dlbuf[0] | (((unsigned int)dlbuf[1]) << 8);
		int length = (int)dlbuf[2] | (((int)dlbuf[3])<<8);
		if(buf_wr_idx == length+6 || buf_wr_idx==PKG_SIZE){
			//			char buf3[50];
			//			sprintf(buf3,"%d/%ld, %lu/%lu, %d\n",pkgnum,numofpkgs-1,downloaded,imagesize,length);
			//			sendString(RADIO,buf3);
			if (!checksum(dlbuf,length)){
				//				sendString(RADIO,"^CHECKSUM ERROR^\n");
				while(unread(CAMERA)){readChar(CAMERA);};
				pkgnum--;
			}else{
				downloaded+=length;
				UINT written;
				f_write(&snapfile,dlbuf+4,length,&written);
			}
			if (downloaded >= imagesize){ // At end of data
				//				sendString(RADIO,"DONE\n");
				f_close(&snapfile);
				char buf[]="RDY";
				createPacket(buf);
				sendData(RADIO,buf,strlen(buf)); // RDY!
				CansatState=POSTFLIGHT;
				CameraState=IDLE;
				if (imagesize!=downloaded){
					//					sendString(RADIO,"Invalid Pkg\n");
				}
				sendACK(0,0xF0F0);
				CameraState=IDLE;
			}else{
				sendACK(0,++pkgnum); // Send for next package
			}

			buf_wr_idx=0;
		}
	}else{
		dl_timeout++;
		if (dl_timeout > 20){
			sendACK(0,pkgnum);
			dl_timeout=0;
		}
	}
}

char checksum(char buf[], int length){
	int j=0;
	unsigned int check=0;
	if (length > PKG_SIZE -6){
		return 0;
	}
	for (;j<length+4;j++){
		check+=dlbuf[j];
	}
	check&=0xFFU;
	unsigned char pkgcheck=dlbuf[length+4];
	if (check != pkgcheck){
		return 0;
	}
	return 1;
}


void getCamCommand(unsigned char str[]){
	int i=0;
	char b=getBlocking();
	setBlocking(NONBLOCK);
	for (i=0;i<6;i++){
		unsigned char c = readChar(CAMERA);
		if (c == 0xAA){
			i=0;
		}
		*(str+i)=c;
	}		
	setBlocking(b);
}

void parseCamCommand(unsigned char str[]){
	if (str[0] != 0xAA){
		return;
	}
	switch (str[1]){
		case 0x0A:
			// Data
			setCamTimeout(0);
			imagesize=(((unsigned int)str[3]) + (((unsigned int)str[4])<<8) + (((long)str[5])<<16));
			numofpkgs=imagesize/(PKG_SIZE-6);
			pkgnum=0;
			CameraState=DOWNLOAD;
			sendACK(0,0); // Initialize download
			break;

		case 0x0D: // SYNC
			sendACK(0x0D,0);
			CameraState=SYNCED;
			break;
		case 0x0E: // ACK
			if (CameraState==INITIALIZING){
				if (str[2]!=0x01){
					sendInit();
				}else{
					setCamTimeout(0);
					CameraState=IDLE;
					setStatus(OKAY);
				}
			}else if (CameraState==SNAPPING){
				if (str[2]==0x05){ // Snapshot success
					getSnap();
				}else if (str[2]==0x04){ // Get picture success
					setCamTimeout(0);
				}else{
					CameraState=IDLE;
					takeSnap();
				}
			}else if (CameraState==UNSYNCED){
				CameraState=SYNCED; // Oh hay, we're synced now.
				delayms(250);
			}
			break;
		case 0x0F: // NAK
			{
				//char ubuf[50];
				//sprintf(ubuf,"N: %X %X %X %X %X\n", str[1],str[2],str[3],str[4],str[5]);
				//sendString(RADIO,ubuf);
				setStatus(CAMERA_NAK);
				switch(str[4]){
					case 0xF0: //Command header error
						CameraState=UNSYNCED;
						// Error, error. Bweeep.
						setCamTimeout(0);
						delayms (10000);
						//					char tmp[6]={0xAA,0x0F,0,0,0xF0,0};
						//					sendData(CAMERA,tmp,6);

						//sendInit();
						break;
				}
			}
			delayms(1000);
			break;
		case 0x08: // RESET
			setStatus(CAMERA_UNSYNCABLE);
			CameraState=UNSYNCED;
			break;
		default: // Unknown packet
			{
				switch (CameraState){
					case SNAPPING:
						takeSnap(); // Resend snap command
						break;
				}
			}
	}

}

void initCamera(){
	CameraState=UNSYNCED;
	enableCamera();
	return;
}


void sendInit(){
	char tmp[6]={0xAA, 0x01, 0x00, 0x07, 0x01, 0x07};
	sendCmd(tmp);
	setCamTimeout(1000);
}

void sendACK(char cmdID, unsigned int pkgID){
	char tmp[6]={0xAA, 0x0E, cmdID, 0, (char)(pkgID & 0x00FF), (char)(pkgID >> 8)};
	sendCmd(tmp);
}

void sendSYNC(){
	if (sync_attempts==120){
		setCamTimeout(0);
		delayms (10000);
		sync_attempts=0;
	}
	char tmp[6]={0xAA,0x0D,0,0,0,0};
	sendCmd(tmp);
	sync_attempts++;
}

void takeSnap(){
	if (CameraState==IDLE && pkgnum == 0){
		openCamFile();
		delayms(3000); //Hogs. Do this delay differently.
		char tmp[6]={0xAA,0x05,0,0,0,0};
		sendCmd(tmp);
		CameraState=SNAPPING;
	}
}

void getSnap(){
	char tmp[6]={0xAA,0x04,0x01,0,0,0};
	sendCmd(tmp);
	setCamTimeout(1000);
}

void sendCmd(char c[]){
	sendData(CAMERA,c,6);
	for (int i=0; i<6; i++){
		lastcmd[i]=c[i];
	}
}


void openCamFile(){
	int i=0;
   FRESULT r = FR_EXIST;
   while (r!=FR_OK){ // Find next unopened log
      char tmpbuf[20]="/moo.jpg";
      //sprintf(tmpbuf,"/snap%04d.jpg",i); // Zero padded tastiness
		strcpy(snapfilename,tmpbuf); // Save filename for later
      r = f_open(&snapfile,tmpbuf,FA_CREATE_NEW | FA_READ | FA_WRITE);
      i++;
   }
}


