#include <avr/io.h>
#include "SD.h"
#include "../main.h"
#include "../USART.h"
#include "../Diagnostics.h"
#include "diskio.h"
#include "ff.h"
#include <util/delay.h>


FATFS fs;

void initSD(){
	// Initialize pins
	initSPI();
	DSTATUS d=STA_NOINIT;
	setStatus(SDFAIL);
	while (d == STA_NOINIT || d==STA_NODISK){
//		sendString(RADIO,"Initializing\n");
		_delay_ms(250); // Wait
		d=disk_initialize(0);
	}
	if (d != STA_NOINIT && d != STA_NODISK){		
//		sendString(RADIO,"Initialized\n");
	}
	FRESULT r;
//	sendString(RADIO,"Mounting... ");
	r = f_mount(0,&fs);
/*	if (r==FR_DISK_ERR){
		sendString(RADIO,"Disk error\n");
	}else if (r==FR_NO_FILESYSTEM){
		sendString(RADIO,"No FAT32\n");
	}else if (r==FR_OK){
		sendString(RADIO,"OKAY!\n");
	}else if (r==FR_NOT_READY){
		sendString(RADIO,"Not ready!\n");
	}else{
		sendString(RADIO,"What?\n");
	}*/
	
	sendString(RADIO,"Opening... ");
	FIL file;
	r=f_open(&file, "/meow.txt",FA_READ);
/*	if (r==FR_NOT_ENABLED){
		sendString(RADIO,"Not enabled\n");
	}else if (r==FR_NO_FILE){
		sendString(RADIO,"No file\n");
	}else if (r==FR_DISK_ERR){
		sendString(RADIO,"Disk Err\n");
	}else if (r==FR_NO_PATH){
		sendString(RADIO,"No path\n");
	}else if(r==FR_OK){
		sendString(RADIO,"OKAY!\n");
	}else{
		sendString(RADIO,"What?\n");
	}*/
	
	delayms(50);
	char Line[100];
	UINT s1;
//	sendString(RADIO,"Reading... ");
/*	for (int i=0;i<20;i++){
		r = f_read(&file,&Line, 1, &s1);
		if (r==FR_OK){
			sendChar(RADIO,Line[0]);
		}
	}*/
	f_read(&file,&Line,100,&s1);
	Line[s1]=0;
	sendString(RADIO,Line);
/*	if (r==FR_NOT_READY){
		sendString(RADIO,"Not Ready\n");
	}else if (r==FR_DISK_ERR){
		sendString(RADIO,"Disk error\n");
	}else if(r==FR_OK){
		sendString(RADIO,"OKAY!\n");
		sendString(RADIO,"---\n");
		sendString(RADIO,Line);
	}else{
		sendString(RADIO,"What?\n");
	}*/


	setStatus(OKAY);
}
DWORD get_fattime(){
	return 0;

}

void initSPI(void){
	// Initialize SPI pins
	SPI_DDR |= _BV(CS) | _BV(MOSI) | _BV(SCK);  // Set as output
	SPI_DDR &= ~_BV(MISO); // MISO is input
	SPI_PORT =  _BV(MISO); // Pull-up resistor

	// Initialize SPI register
	SPCR |= _BV(SPE) | _BV(MSTR) | _BV(SPR1) |   _BV(SPR0);   
	SPCR &= ~_BV(CPOL);  
	SPSR = 0x00; 
}


