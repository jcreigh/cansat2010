#ifndef __SD_H__
#define __SD_H__

#define SPI_PORT PORTB
#define SPI_DDR DDRB
#define CS PB4
#define MOSI PB5
#define MISO PB6
#define SCK PB7

/* Port Controls (Platform dependent) */

#define SELECT()  //SPI_PORT &= ~_BV(CS)      /* MMC CS = L */
#define  DESELECT() // SPI_PORT |=  _BV(CS)      /* MMC CS = H */
#define  MMC_SEL 1    //!(SPI_PORT &  _BV(CS)) /* MMC CS status (true:selected) */



void initSD();
void initSPI(void);
unsigned char SPI_transmit(unsigned char data);
unsigned char SPI_receive(void);

#endif // __SD_H__
