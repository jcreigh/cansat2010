#!/usr/bin/perl
use strict;
use warnings;
open FH, "</dev/ttyUSB0" or die $!;
my $x="";
my $t="";
my $i=0;
while(1){
	$i++;
	my $y=read FH, $x, 1;
	printf (" %.2x", ord($x));
	if (ord($x)<32 || ord($x)>127){
		$x =~ s/ \n/\\n/;
		$x =~ s/ \t/\\t/;
		$x =~ s/ \r/\\r/;
		if (length($x)==1){
			$x=" . ";
		}
	}else{
		$x=" $x ";
	}
	$t .= $x;
	if ($i%10==0){
		print "   $t\n";
		$i=0;
		$t="";
	}elsif ($i%5==0){
		print " ";
		$t.=" ";
	}

}
