#include "Storage.h"
#include "Diagnostics.h"
#include "SD/SD.h"
#include "SD/ff.h"
#include "USART.h"
//#include <string.h>
#include <stdio.h>
#include "main.h"

long storage_idx=0;
FIL logfile;
extern FATFS fs;
char opened=0;

void initStorage(){
	initSD();
	log_open();
}


void log_open(){
	int i=0;
	FRESULT r = FR_EXIST;
	sendString(RADIO,"Opening... ");
	opened=0;
	while (r!=FR_OK){ // Find next unopened log
		char tmpbuf[20];
		sprintf(tmpbuf,"/log%04d.txt",i); // Zero padded tastiness
		sendString(RADIO,tmpbuf);
		r = f_open(&logfile,tmpbuf,FA_CREATE_NEW | FA_READ | FA_WRITE);
		i++;
		delayms(10);
	}
	opened=1;
	sendString(RADIO,"done\n");
}

void log_writeLine(char line[]){
	// Assumes properly seeked
	
//	char buf[10];
//	sprintf(buf,"----- %d\n",strlen(line));
	//sendString(RADIO,buf);
	if (opened){
		UINT written;
		//sendString(RADIO,line);
		f_write(&logfile,line,strlen(line),&written);
	}
}

void log_close(){
	f_close(&logfile);
}

int log_readLine(char line[]){
	// Read up until newline
	if (opened){
		char c[1];
		c[0]=0;
		int i=0;
		UINT read=1;
		while (c[0]!='\n' && read){
			if (f_read(&logfile,c,1,&read)!=FR_OK){
				setStatus(STORAGE_READ_ERR);
				return 0;
			}
			if (read>0){
				line[i++]=c[0];
			}
		}
		line[i-1]=0; // Overwrite the newline
		return i;
	}
	return 0;
}

void log_seekLine(int linenum){
	// Messy and inefficient.
	char line[255]; // Yeah yeah
	int i=0;
	f_lseek(&logfile, 0);
	int x=1;
	while (x && i!=linenum){
		x=log_readLine(line);
		i++;
	}
}

void log_sync(){
	if (opened){
		if (f_sync(&logfile)!=FR_OK){
			setStatus(SDFAIL);
		}
	}
}

char log_opened(){
	return opened;
}
