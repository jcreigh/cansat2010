#include <avr/io.h>
#include <avr/interrupt.h> 
#include "Diagnostics.h"
#include "Telemetry.h"
#include "Storage.h"

unsigned long chronos;

void initChronos(){
	TCCR0A|=_BV(WGM01);
	TCCR0B|= _BV(CS00) | _BV(CS02);
	TIMSK0 |= _BV(OCIE0A);
	OCR0A=(unsigned char)(F_CPU/1024/200);
}

ISR(TIMER0_COMPA_vect){
	//Called about ~5ms, suppoosedly
	chronos++;
	if (chronos % 2 == 0){
		checkStatus();
	}
	if (log_opened()){
		if (chronos % 1000 == 0){ // It's been 5 seconds, send telemetry packet
			sendTelemetry();
		}
		getSensors();
	}
}

