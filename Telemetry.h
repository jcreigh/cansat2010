
#define AVGCOUNT 8
struct enviro_data_struct{
	int voltage[AVGCOUNT+2]; // 8 for the rolling, 1 for the idx, 1 for the avg
	int temp[AVGCOUNT+2];
	int pressure[AVGCOUNT+2];
	int alt; // Stored in decameters
	int accel_x[AVGCOUNT+2];
	int accel_y[AVGCOUNT+2];
	int accel_z[AVGCOUNT+2];
};

#define ACCELS 0b001
#define ENVIRO 0b010
#define GPSPKT 0b100

void sensorLoop();
void createTelemetryPkt(char* string);
void getSensors();
void logGPS();
void genAltitude();
void sensorAverage(int sensor[], int value);
void sendTelemetry();
