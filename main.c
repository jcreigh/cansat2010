#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>

#include "GPS.h"
#include "USART.h"
#include "Telemetry.h"
#include "Storage.h"
#include "SD/SD.h"
#include "Diagnostics.h"
#include "Camera.h"
#include "Chronos.h"
#include "Commands.h"
#include "Sensors.h"

extern char CansatState;

void delayms(uint16_t millis) {
   while ( millis-- ) {
      _delay_ms(1);
   }
}

void init(){
	initUSART();
	initSensors();
	initChronos();
	initStatus();
	initStorage();
	initCommands();
	//initCamera();
	sendString(RADIO,"Initialized\n");
}


int main(void){
	init();
	while(1) {
		switch(CansatState){
			case PREFLIGHT:
			case LAUNCHED:
			case DEPLOYED:
				sensorLoop();
				break;
			case POSTFLIGHT:
				sendString(RADIO,"Postflight?\n");
				setStatus(SDFAIL);
				log_close();
				break;

			case TAKEPHOTO:
				cameraLoop();
				break;
		}
		commandsLoop();
	}

	return 0;
}
