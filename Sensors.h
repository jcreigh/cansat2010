#ifndef __SENSORS_H__
#define __SENSORS_H__

#define ADC_PORT PORTA
#define ADC_DDR DDRA

//The following pin numbers aren't set in stone
#define ACCEL_X 1
#define ACCEL_Y 2
#define ACCEL_Z 3
#define PRESSURE 0
#define TEMP 5
#define VOLTAGE 4

#define ACCEL_PRECISION 1 // Fix me

void initSensors();
int readADC(char sensor);
double convertADC(char sensor, int value);

#endif //__SENSORS_H__
