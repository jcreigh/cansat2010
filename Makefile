OBJS= main.o Camera.o USART.o GPS.o Telemetry.o PWM.o Sensors.o Radio.o Diagnostics.o  SD/SD.o SD/mmc.o SD/ff.o Storage.o Chronos.o Commands.o
CC=avr-gcc
MCU_TARGET=atmega644p
CFLAGS=-Werror -Wall -DF_CPU=8000000 -Os -mmcu=$(MCU_TARGET) -std=c99
TARGET=main.hex
ELFTARGET=main.elf

$(TARGET): $(ELFTARGET)
	avr-strip $<
	avr-objcopy -R .eeprom  -O ihex $< $@

$(ELFTARGET): $(OBJS) 
	$(CC) $(CFLAGS) $(OBJS) -o $(ELFTARGET)

%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

%.o: SD/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: install clean size

clean:
	rm $(OBJS)
	rm $(TARGET)
	rm $(ELFTARGET)


install:
	@echo  "Installing... "
	@avrdude -c usbtiny -p m644p -U flash:w:$(TARGET)


size: 
	@avr-size -A $(ELFTARGET)

doall:  $(TARGET) size install
